import sys
import os
sys.path.insert(0, '..')

from dynamics1D.potential import *
from dynamics1D.quantum import *
from pythonplot.movie import mkmovie

# Création du dossier pour stocker les images
if not os.path.isdir("pics"): 
	os.mkdir("pics")
else:
	os.system("rm -rf pics")
	os.mkdir("pics")

# Paramètres
h=0.1
gamma=0.25
e=1.0
N=64*2

# Création des instances
grid=Grid(N,h) # grille
pot=DoubleWell(e,gamma) # potentiel 
ham=Hamiltonian(grid,pot=pot) # hamiltonien 
tp=TimePropagator(grid,ham,dt=1) # propagateur temporel sur dt=1
husimi=Husimi(grid,scale=6) # husimis

# Diagonalisation de l'hamiltonien
ham.diagonalize() 
# Construction d'un état gauche/droite à partir du doublet symétrique/antisymétrique
wf=(ham.eigenvec[0]+ham.eigenvec[1])/np.sqrt(2)
# Calcul de la période attendue d'oscillation
T=int(2*np.pi*h/(np.abs(ham.eigenval[0]-ham.eigenval[1])))

# Génération d'un husimi tous les icheck
icheck=int(T/25)

for i in range(T):
	# Propagation de la fonction d'onde
	wf=tp%wf
	
	# Génération du husimi
	if i%icheck==0:
		husimi.compute(wf)
		husimi.savePNG(PPfile="pp-husimi",pngfile="pics/{:05d}".format(int(i/icheck)))
		
		
mkmovie("pics/",5)

