import sys
import os
sys.path.insert(0, '..')

from dynamics1D.potential import *
from dynamics1D.quantum import *


h=0.2
omega=0.5
N=64*2

grid=Grid(N,h)
pot=HarmonicOscillator(omega)
ham=Hamiltonian(grid,pot=pot)
ham.diagonalize()

ax=plt.gca()
ax.plot(grid.x,pot.Vx(grid.x)/(h*omega))
for i in range(10):
	E=ham.eigenval[i]/(h*omega)
	ax.plot(grid.x,np.ones(N)*E)
	ax.plot(grid.x,0.7*np.abs(ham.eigenvec[i].x)**2/np.max(np.abs(ham.eigenvec[i].x)**2)+E)
plt.show()











